const express = require("express");
const { addCandidate, getAllCandidate } = require("../controller/candidateController");
const { admin } = require("../middleware/adminmiddleware");
const { auth } = require("../middleware/authmiddleware");

const candidateRouter = express.Router();


candidateRouter.post('/add',auth,admin, addCandidate)
candidateRouter.get('/get',auth,admin, getAllCandidate)



module.exports={
    candidateRouter
}