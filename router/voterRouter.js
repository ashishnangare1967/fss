const express = require("express");
const bcrypt = require("bcrypt");


var jwt = require('jsonwebtoken');
const { voterModel } = require("../model/voterModel");
const { voteCandidate } = require("../controller/voteController");
const { auth } = require("../middleware/authmiddleware");

const voterRouter = express.Router();

voterRouter.post("/register", async (req, res) => {
    const {userName, email, pass, phone}= req.body;
    const user = await voterModel.findOne({ email: email });
    if(user){
     res.send("a new user with same email cannot register")
    }else{
     try {
       bcrypt.hash(pass, saltRounds=5, async function(err, hash) {
          if(err){
             res.send(400).send({msg:err.message})
          }else{
             const user = voterModel({userName, email, phone, pass:hash});
             await user.save();
        res.status(200).json({ msg: "New voter has been added", registeredUser: req.body });
          }
      });
      } catch (err) {
        res.status(400).send({ error: err.message });
      }
    }
   
 });


 voterRouter.post("/login", async (req, res) => {
    const { email, pass } = req.body;
  let oldpass=pass
    try {
      const user = await voterModel.findOne({ email: email });
      if (user) {
        bcrypt.compare(pass, user.pass, function(err, result) {
           if(result){
              var token = jwt.sign({ userID: user._id, userName: user.userName, pass }, 'masai');
             res.status(200).json({msg:'Login successful!', token})
           }else{
              res.status(200).send({msg:"Wrong credentials"})
           }
       });
     
      } else {
        res.status(200).send({ msg: "User Not Found" });
      }
    } catch (err) {
      res.status(400).send({ error: err.message });
    }
  });


  voterRouter.post('/addvote',auth, voteCandidate)
  





module.exports={
    voterRouter
}
