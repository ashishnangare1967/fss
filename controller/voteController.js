const candidateModel = require("../model/candidateModel");
const voteModel = require("../model/voteModel");
const { voterModel } = require("../model/voterModel");

exports.voteCandidate = async (req, res) => {
    try {
      const { candidateID, userID, userName } = req.body;

      const getvotes= await voteModel.find({userID:req.body.userID})
    //   console.log(">>>>", getvotes)
      if(getvotes.length>0){
        res.send({msg:"User has already voted"})
      }else{
        const vote = new voteModel({
            candidateID, userID, userName
          });
          const savedVote = await vote.save();
          const udpatedCandidate = await candidateModel.findByIdAndUpdate(
            candidateID,
            { $push: { votes: savedVote._id } },
            { new: true }
          )
          res.send({
            post:  udpatedCandidate,
          });
      }
    //   if(getvotes)


    } catch (error) {
      return res.status(400).json({
        error: "Error while Liking post",
      });
    }
  };