const mongoose = require("mongoose");

const voterSchema= mongoose.Schema({
    userName:String,
    email:String,
    pass: String,
    phone:Number,
}, {
    versionKey:false
})


const voterModel= mongoose.model('User', voterSchema)


module.exports={
    voterModel
}