const mongoose = require("mongoose");

const candidateSchema= mongoose.Schema(
    {
        name:{
            type: String,
            required: true,
        },
        votes: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Vote",
        }],
    }
)

module.exports = mongoose.model("Candidate", candidateSchema);