//import mongoose
const mongoose = require("mongoose");


//route handler

const voteSchema = new mongoose.Schema({
    candidateID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Candidate"
    },
    userID: {
        type: String,
        required:true,
    },
    userName: {
        type: String,
        required:true,
    }
});


const voteModel= mongoose.model('Vote', voteSchema)

//export
module.exports = voteModel