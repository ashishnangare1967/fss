const express = require("express");
const bcrypt = require("bcrypt");

var jwt = require("jsonwebtoken");

const auth = async (req, res, next) => {
  const token = req.headers.authorization?.split(" ")[1];
  if (token) {
    try {
      jwt.verify(token, "masai", (err, decoded) => {
        if (decoded) {
          req.body.userID = decoded.userID;
          req.body.userName = decoded.userName;
          next();
        } else {
          res.send({ msg: "token is not recognixed " });
        }
      });
    } catch (err) {
      res.json({ msg: err.message });
    }
  } else {
    res.send({ msg: "Please Login" });
  }
};

module.exports = {
  auth,
};
